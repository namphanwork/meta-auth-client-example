import React, { Component } from "react";
import "./App.css";

import getWeb3 from "./getWeb3";

class App extends Component {
  constructor() {
    super()
    this.state = { web3: null, accounts: null, challenge: null, signature: null, token: null };
    this.getChallenge = this.getChallenge.bind(this)
    this.signChallenge = this.signChallenge.bind(this)
    this.verifySignature = this.verifySignature.bind(this)
  }

  async componentDidMount() {
    const web3 = await getWeb3()
    console.log(web3)
    const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
    console.log(accounts)
    this.setState({ web3, accounts })
  }

  async getChallenge() {
    console.log(this.state)
    const { accounts } = this.state;
    console.log(accounts)
    const res = await fetch(
      `http://167.172.82.210:3000/user/auth/${accounts[0].toLowerCase()}`
    );
    this.setState({ challenge: await res.json() });
  };

  async signChallenge() {
    const { web3, challenge, accounts } = this.state;
    web3.currentProvider.sendAsync(
      {
        method: "eth_signTypedData",
        params: [challenge, accounts[0]],
        from: accounts[0]
      },
      (error, res) => {
        if (error) return console.error(error);
        this.setState({ signature: res.result });
      }
    );
  };

  async verifySignature() {
    const { challenge, signature, accounts } = this.state;
    const res = await fetch(
      `http://167.172.82.210:3000/user/auth/${challenge[1].value}/${signature}`
    );
    const data = await res.json();
    if (res.status === 200) {
      console.log("Signature verified");
      this.setState({token: data.data})
    } else {
      console.log("Signature not verified");
    }
  };

  render() {
    const { web3, challenge, signature, token } = this.state;
    if (!web3) return "Loading...";
    return (
      <div className="App">
        <button onClick={this.getChallenge}>Get Challenge</button>
        <button onClick={this.signChallenge} disabled={!challenge}>
          Sign Challenge
        </button>
        <button onClick={this.verifySignature} disabled={!signature}>
          Verify Signature
        </button>

        {challenge && (
          <div className="data">
            <h2>Challenge</h2>
            <pre>{JSON.stringify(challenge, null, 4)}</pre>
          </div>
        )}

        {signature && (
          <div className="data">
            <h2>Signature</h2>
            <pre>{signature}</pre>
          </div>
        )}

        {token && (
          <div className="data">
            <h2>JWT token</h2>
            <pre>{token}</pre>
          </div>
        )}
      </div>
    );
  }
}

export default App;
